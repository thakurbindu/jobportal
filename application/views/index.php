
<!--Search Block-->
<div class="top-colSection">
  <div class="container">
    <div class="row">
      

  <div class="candidatesection">
  
  <h1>One million success stories. <span>Start yours today.</span></h1>
  
  <form action="http://codeareena.com/demo/jobportal/job_search/search" name="jsearch" id="jsearch" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    <div class="col-md-6">
          <input type="text" required name="job_params" id="job_params" class="form-control" placeholder="Job title or Skill" />                
    </div>
    
    <div class="col-md-4">
      <select class="form-control" name="jcity" id="jcity">
      	
        <option value="" selected>Select City</option>
                	<option value="Austin">Austin</option>
                	<option value="Boston">Boston</option>
                	<option value="California">California</option>
                	<option value="Chicago">Chicago</option>
                	<option value="Dubai">Dubai</option>
                	<option value="Hong Kong">Hong Kong</option>
                	<option value="Houston">Houston</option>
                	<option value="Islamabad">Islamabad</option>
                	<option value="Lahore">Lahore</option>
                	<option value="Las Vegas">Las Vegas</option>
                	<option value="Los Angeles">Los Angeles</option>
                	<option value="New York">New York</option>
                	<option value="San Francisco">San Francisco</option>
                	<option value="Sydney">Sydney</option>
                	<option value="Washington">Washington</option>
              </select>
    </div>

    <div class="col-md-2">
      <input type="submit" name="job_submit" class="btn" id="job_submit" value="Search"  />
    </div>
</form> 
    <div class="clear"></div>
  </div>





  <div class="employersection">   
      <h3>Get Started Now</h3>
      <a href="employer/post_new_job.html" class="postjobbtn" title="USA jobs">Post a Job</a>
      <input type="submit" value="Upload Resume" title="job search engine USA" class="btn" alt="job search engine USA" onClick="document.location='login.html'" />
      <div class="clear"></div>
    </div>


      <div class="clear"></div>
    </div>
  </div>
</div>
<!--/Search Block--> 


<!--Latest Jobs Block-->
<div class="latestjobs">
<div class="container">
       
          <div class="titlebar">           
              <h2>Latest Jobs</h2>
              <strong>Total - 137</strong>           
          </div>
          
          
          <ul class="row joblist">
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/xcv-company.html" title="Jobs in XCV Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457769102.jpg" alt="http://codeareena.com/demo/jobportal/company/xcv-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/xcv-company-jobs-in-las%20vegas-hr-specilest-15.html" class="jobtitle" title="Hr Specilest">Hr Specilest</a> <span><a href="company/xcv-company.html" title="Jobs in XCV Company">XCV Company</a> &nbsp;-&nbsp; Las Vegas</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="HR / Industrial Relations Job in Las Vegas">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/asd-company.html" title="Jobs in ASD Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457768887.jpg" alt="http://codeareena.com/demo/jobportal/company/asd-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/asd-company-jobs-in-los%20angeles-call-center-operator-14.html" class="jobtitle" title="Call Center Operator">Call Center Operator</a> <span><a href="company/asd-company.html" title="Jobs in ASD Company">ASD Company</a> &nbsp;-&nbsp; Los Angeles</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="Customer Service Job in Los Angeles">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/qwe-company.html" title="Jobs in QWE Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457768561.jpg" alt="http://codeareena.com/demo/jobportal/company/qwe-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/qwe-company-jobs-in-new%20york-account-officer-13.html" class="jobtitle" title="Account Officer">Account Officer</a> <span><a href="company/qwe-company.html" title="Jobs in QWE Company">QWE Company</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="HR / Industrial Relations Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/mnf-comapny.html" title="Jobs in MNF Comapny" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457713999.jpg" alt="http://codeareena.com/demo/jobportal/company/mnf-comapny" /></a></div>
                <div class="col-xs-6"> <a href="jobs/mnf-comapny-jobs-in-new%20york-web-design-frontend-developer-12.html" class="jobtitle" title="Web Design / Frontend Developer">Web Design / Frontend Developer</a> <span><a href="company/mnf-comapny.html" title="Jobs in MNF Comapny">MNF Comapny</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="Graphic / Web Design Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/mnt-comapny.html" title="Jobs in MNT Comapny" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457713426.jpg" alt="http://codeareena.com/demo/jobportal/company/mnt-comapny" /></a></div>
                <div class="col-xs-6"> <a href="jobs/mnt-comapny-jobs-in-islamabad-seo-specialist-11.html" class="jobtitle" title="Seo Specialist">Seo Specialist</a> <span><a href="company/mnt-comapny.html" title="Jobs in MNT Comapny">MNT Comapny</a> &nbsp;-&nbsp; Islamabad</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="Advertising Job in Islamabad">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/mno-company.html" title="Jobs in Mno Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457713172.jpg" alt="http://codeareena.com/demo/jobportal/company/mno-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/mno-company-jobs-in-new%20york-front-end-developers-10.html" class="jobtitle" title="Front End Developers">Front End Developers</a> <span><a href="company/mno-company.html" title="Jobs in Mno Company">Mno Company</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/jkl-company.html" title="Jobs in Jkl Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457712255.jpg" alt="http://codeareena.com/demo/jobportal/company/jkl-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/jkl-company-jobs-in-new%20york-graphic-designer-9.html" class="jobtitle" title="Graphic Designer">Graphic Designer</a> <span><a href="company/jkl-company.html" title="Jobs in Jkl Company">Jkl Company</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/ghi-company.html" title="Jobs in Ghi Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457711897.jpg" alt="http://codeareena.com/demo/jobportal/company/ghi-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/ghi-company-jobs-in-dubai-teachers-and-administration-staff-8.html" class="jobtitle" title="Teachers And Administration Staff">Teachers And Administration Staff</a> <span><a href="company/ghi-company.html" title="Jobs in Ghi Company">Ghi Company</a> &nbsp;-&nbsp; Dubai</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="Advertising Job in Dubai">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/def-it-company.html" title="Jobs in Def It Company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457711477.jpg" alt="http://codeareena.com/demo/jobportal/company/def-it-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/def-it-company-jobs-in-new%20york-graphic-designer-adobe-indesign-expert-7.html" class="jobtitle" title="Graphic Designer Adobe Indesign Expert">Graphic Designer Adobe Indesign Ex&hellip;</a> <span><a href="company/def-it-company.html" title="Jobs in Def It Company">Def It Company</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/abc-it-tech.html" title="Jobs in Abc IT Tech" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457711170.jpg" alt="http://codeareena.com/demo/jobportal/company/abc-it-tech" /></a></div>
                <div class="col-xs-6"> <a href="jobs/abc-it-tech-jobs-in-dubai-head-of-digital-marketing-6.html" class="jobtitle" title="Head Of Digital Marketing">Head Of Digital Marketing</a> <span><a href="company/abc-it-tech.html" title="Jobs in Abc IT Tech">Abc IT Tech</a> &nbsp;-&nbsp; Dubai</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="Advertising Job in Dubai">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/some-it-company.html" title="Jobs in Some IT company" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457693358.jpg" alt="http://codeareena.com/demo/jobportal/company/some-it-company" /></a></div>
                <div class="col-xs-6"> <a href="jobs/some-it-company-jobs-in-hong%20kong-front-end-developer-5.html" class="jobtitle" title="Front End Developer">Front End Developer</a> <span><a href="company/some-it-company.html" title="Jobs in Some IT company">Some IT company</a> &nbsp;-&nbsp; Hong Kong</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in Hong Kong">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/info-technologies.html" title="Jobs in Info Technologies" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457691226.jpg" alt="http://codeareena.com/demo/jobportal/company/info-technologies" /></a></div>
                <div class="col-xs-6"> <a href="jobs/info-technologies-jobs-in-sydney-dot-net-developer-4.html" class="jobtitle" title="Dot Net Developer">Dot Net Developer</a> <span><a href="company/info-technologies.html" title="Jobs in Info Technologies">Info Technologies</a> &nbsp;-&nbsp; Sydney</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in Sydney">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/it-pixels.html" title="Jobs in It Pixels" class="thumbnail"><img src="public/uploads/employer/thumb/JOBPORTAL-1457690733.jpg" alt="http://codeareena.com/demo/jobportal/company/it-pixels" /></a></div>
                <div class="col-xs-6"> <a href="jobs/it-pixels-jobs-in-new%20york-java-developer-3.html" class="jobtitle" title="Java Developer">Java Developer</a> <span><a href="company/it-pixels.html" title="Jobs in It Pixels">It Pixels</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                        <li class="col-md-6">
              <div class="intlist">
                <div class="col-xs-2"><a href="company/mega-technologies.html" title="Jobs in Mega Technologies" class="thumbnail"><img src="public/uploads/employer/thumb/no_pic.jpg" alt="http://codeareena.com/demo/jobportal/company/mega-technologies" /></a></div>
                <div class="col-xs-6"> <a href="jobs/mega-technologies-jobs-in-new%20york-web-designer-1.html" class="jobtitle" title="Web Designer">Web Designer</a> <span><a href="company/mega-technologies.html" title="Jobs in Mega Technologies">Mega Technologies</a> &nbsp;-&nbsp; New York</span> </div>
                <div class="col-xs-4"> <a href="login.html?apply=yes" class="applybtn" title="IT - Software Job in New York">Apply Now</a> </div>
                <div class="clear"></div>
              </div>
            </li>
                      </ul>

</div>
</div>
<!--/Latest Jobs Block--> 




<!--Featured Jobs-->      
<div class="featuredWrap" >
<div class="container">
    <div class="titlebar"> <h2>Featured Jobs</h2></div>
    	<ul class="featureJobs row">
                    <li class="col-md-6">
          	<div class="intbox">
            <div class="compnyinfo">
            <a href="jobs/info-technologies-jobs-in-sydney-dot-net-developer-4.html" title="Dot Net Developer">Dot Net Developer</a> <span><a href="company/info-technologies.html" title="Jobs in Info Technologies">Info Technologies</a> &nbsp;-&nbsp; Sydney</span> </div>
            <div class="date">Apply by <br />
              Jul 11, 2016</div>
            <div class="clear"></div>
            </div>
          </li>
                    <li class="col-md-6">
          	<div class="intbox">
            <div class="compnyinfo">
            <a href="jobs/it-pixels-jobs-in-new%20york-java-developer-3.html" title="Java Developer">Java Developer</a> <span><a href="company/it-pixels.html" title="Jobs in It Pixels">It Pixels</a> &nbsp;-&nbsp; New York</span> </div>
            <div class="date">Apply by <br />
              Jul 11, 2016</div>
            <div class="clear"></div>
            </div>
          </li>
                    <li class="col-md-6">
          	<div class="intbox">
            <div class="compnyinfo">
            <a href="jobs/mega-technologies-jobs-in-new%20york-web-designer-1.html" title="Web Designer">Web Designer</a> <span><a href="company/mega-technologies.html" title="Jobs in Mega Technologies">Mega Technologies</a> &nbsp;-&nbsp; New York</span> </div>
            <div class="date">Apply by <br />
              Jul 11, 2016</div>
            <div class="clear"></div>
            </div>
          </li>
		  <li class="col-md-6">
          	<div class="intbox">
            <div class="compnyinfo">
            <a href="jobs/it-pixels-jobs-in-new%20york-java-developer-3.html" title="Java Developer">Java Developer</a> <span><a href="company/it-pixels.html" title="Jobs in It Pixels">It Pixels</a> &nbsp;-&nbsp; New York</span> </div>
            <div class="date">Apply by <br />
              Jul 11, 2016</div>
            <div class="clear"></div>
            </div>
          </li>
                  </ul>
</div>
</div>
<!--Featured Jobs End-->


