         <div class="container detailinfo">
            <div class="container">
               <div class="col-xs-12 sign-Up">
                  <div class="stepwizard">
                     <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                           <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                           <p>Step 1</p>
                        </div>
                        <div class="stepwizard-step">
                           <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                           <p>Step 2</p>
                        </div>
                        <div class="stepwizard-step">
                           <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                           <p>Step 3</p>
                        </div>
                        <div class="stepwizard-step">
                           <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                           <p>Step 4</p>
                        </div>
						<div class="stepwizard-step">
                           <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                           <p>Step 5</p>
                        </div>
                     </div>
                  </div>
                  <form role="form" action="" method="post">
                     <div class="row setup-content" id="step-1">
                        <div class="col-xs-12 ">
                           <div class="col-md-6 sign_up_text">
                              <p>*Our team will review your resume and if required suitable enhancement/editing would be made to improve your chances to get you to the interview table.</p>
                              <p>*Improved resume will be shared with you and upon your confirmation the same will be stored in our database for employer access</p>
                              <p>*Access to exclusive job openings.</p>
                              <p>*Only relevant jobs opening will be forwarded to you</p>
                           </div>
                           <div class="col-md-6 sign_up_form">
                              <div class="form-group"><a  href="index.html"><img src="public/images/facebook_btn.png" /></a>
                                 <a  href="index.html"><img src="public/images/gmail_icon.jpg" /></a>
                              </div>
                              <div class="form-group">
                                 <input  maxlength="100" type="text" class="form-control" placeholder="Email id"  />
                              </div>
                              <div class="form-group">
                                 <input maxlength="100" type="text" class="form-control" placeholder="Confirm your email id" />
                              </div>
                              <div class="form-group">
                                 <input type="password" name="pass" id="pass" autocomplete="off" value="" class="form-control" placeholder="Password" />
                              </div>
                              <div class="form-group">
                                 <p>I have read and agreed to jobreceipt.com terms of use and Privacy Policy</p>
                              </div>
                              <div class="form-group"> <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Submit & Continue</button></div>
                              <p>Already a member ?<a href="#">Login Here</a>
                           </div>
                        </div>
                     </div>
                     <div class="row setup-content" id="step-2">
                        <div class="col-md-12 sign_up_form step_2">
                           <div class="col-md-12">
                              <h3> CONCISE VIEW</h3>
                              <div class="form-group col-md-6">
                                 <div class="col-md-6">
                                    <label class="control-label ">Total years of experience</label>
                                    <div class=" col-md-6">
                                       <input maxlength="200" type="text" required="required" class="form-control" placeholder="YEARS" />
                                    </div>
                                    <div class=" col-md-6">
                                       <input maxlength="200" type="text" required="required" class="form-control" placeholder="Months" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Industry Experience</label>
                                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="﻿"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Skill Set</label>
                                    <div class=" col-md-4">
                                       <input maxlength="200" type="text" required="required" class="form-control" placeholder="skill 1" />
                                    </div>
                                    <div class=" col-md-4">
                                       <input maxlength="200" type="text" required="required" class="form-control" placeholder="skill 2" />
                                    </div>
                                    <div class=" col-md-4">
                                       <input maxlength="200" type="text" required="required" class="form-control" placeholder="skill 3" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Upload your resume</label>
                                    <div class="input-group">
                                       <div class="input-group-prepend">
                                          <div class="custom-file">
                                             <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group col-md-12 Submit_btn">
                                    <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Submit & Continue</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row setup-content" id="step-3">
                        <div class="col-md-12 sign_up_form step_2">
                           <div class="col-md-12">
                              <h3> PERSONAL DETAIL</h3>
                              <div class="form-group col-md-6">
                                 <div class="col-md-6">
                                    <label class="control-label ">First Name</label>
                                    <input maxlength="200" type="text" class="form-control" placeholder="First Name﻿"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Last Name</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="﻿Last Name"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Email ID 1 ( used for register as username)</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="﻿Email ID 1 ( used for register as username)"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Additional Email id </label>
                                    <input maxlength="200" type="text" class="form-control" placeholder="Additional Email id "  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Contact No.</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">marital Status</label>
                                    <select class="form-control" id="sel1">
                                       <option>marital Status</option>
                                       <option>2</option>
                                       <option>3</option>
                                       <option>4</option>
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">DOB </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="DOB"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Languages Known </label>
                                    <select class="form-control" id="sel1">
                                       <option>Languages Known </option>
                                       <option>Hindi</option>
                                       <option>English</option>
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Nationality </label>
                                    <select class="form-control" id="sel1">
                                       <option>select Country</option>
                                       <option>2</option>
                                       <option>3</option>
                                       <option>4</option>
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Passport no.  </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=" "  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Passport Expiry Date  </label>
                                    <input maxlength="200" type="text" class="form-control" placeholder=""  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">What is your visa status in ( name of the country). </label>
                                    <input maxlength="200" type="text" class="form-control" placeholder="Passport no. "  />
                                 </div>
                                 <div class="form-group col-md-12 Submit_btn">
                                    <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Submit & Continue</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row setup-content" id="step-4">
                        <div class="col-md-12 sign_up_form step_2">
                           <div class="col-md-12">
                              <h3> WORK EXPERIENCE</h3>
                              <div class="form-group col-md-6">
                                 <div class="col-md-6">
                                    <label class="control-label ">Current Designation</label>
                                    <input maxlength="200" type="text" class="form-control" placeholder="﻿"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Designation</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Current Employer </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="Company Name"  />
                                    <div class=" col-md-6">
                                       <label class="control-label ">From </label>
                                       <input maxlength="200" type="text" class="form-control" placeholder="From" />
                                    </div>
                                    <div class=" col-md-6">
                                       <label class="control-label ">To </label>
                                       <input maxlength="200" type="text" class="form-control" placeholder="To" />
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <label class="control-label ">Current work Location</label>
                                    <div class=" col-md-6">
                                       <input maxlength="200" type="text"  class="form-control" placeholder="Country" />
                                    </div>
                                    <div class=" col-md-6">
                                       <input maxlength="200" type="text"  class="form-control" placeholder="Cities of that country" />
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Industry </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=" "  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Job Type</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
								   <div class="bg_class">
                                 <div class="col-md-6">
                                    <label class="control-label ">Job Designation</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
									<label class="control-label ">Industry </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Employer Name </label>
                                    <input maxlength="200" type="text" class="form-control" placeholder=""  />
                                    <div class=" col-md-6">
                                       <label class="control-label ">From </label>
                                       <input maxlength="200" type="text" class="form-control" placeholder="From" />
                                    </div>
                                    <div class=" col-md-6">
                                       <label class="control-label ">To </label>
                                       <input maxlength="200" type="text" class="form-control" placeholder="To" />
                                    </div>
                                 </div>

								 </div>
								  <h5 class="add_data">+ Add Previous Employer</h5>

							   <div class="bg_class">
                                 <div class="col-md-6">
                                    <label class="control-label ">Block companies to see your resume </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
								 </div>
								   <h5 class="add_data">Unblock company</h5>
                                 <div class="form-group col-md-12 Submit_btn">
                                    <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Submit & Continue</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

					  <div class="row setup-content" id="step-5">
                        <div class="col-md-12 sign_up_form step_2">
                           <div class="col-md-12">
                              <h3> QUALIFICATION</h3>
                              <div class="form-group col-md-6">
							  <div class="bg_class">
                                 <div class="col-md-6">
                                    <label class="control-label ">What is your highest Qulification</label>
                                    <select class="form-control" id="sel1">
                                       <option>Select Qualification</option>
                                       <option>High Schooling</option>
                                       <option>Graduation</option>
                                       <option>Post graduation</option>
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Course Details</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="Course Details"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Specilization/ major </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder="Years of Passing"  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Any project/ Internship</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />

                                 </div>
								 </div>
								 <h5 class="add_data">+ Add other qualification/ Certification</h5>

								  <h3 class="title_here"> Expectation and other infomations</h3>

                                 <div class="col-md-6">
                                    <label class="control-label ">Expected Salary </label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=" "  />
                                 </div>
                                 <div class="col-md-6">
                                    <label class="control-label ">Notice Period to join new company</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
                                 <div class="col-md-12 padding_0">
                                    <label class="control-label ">Is there any other information that can help employer to know about you and your work?</label>
                                    <input maxlength="200" type="text"  class="form-control" placeholder=""  />
                                 </div>
                                 <div class="form-group col-md-12 Submit_btn">
                                    <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Submit</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </form>
               </div>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="text-center"></div>
         </div>
      </div>
      <!--Footer-->
      <div class="footerWrap">
         <div class="container">
            <div class="col-md-3">
               <img src="public/images/logo.png" alt="Job Portal" />
               <br><br>
               <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
               <!--Social-->
               <div class="social">
                  <a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                  <a href="http://www.plus.google.com/" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                  <a href="http://www.facebook.com/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                  <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
                  <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                  <a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
               </div>
            </div>
            <div class="col-md-2">
               <h5>Quick Links</h5>
               <ul class="quicklinks">
                  <li><a href="about-us.html" title="About Us">About Us</a></li>
                  <li><a href="how-to-get-job.html" title="How to get Job">How to get Job</a></li>
                  <li><a href="search-jobs.html" title="New Job Openings">New Job Openings</a></li>
                  <li><a href="search-resume.html" title="New Job Openings">Resume Search</a></li>
                  <li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h5>Popular Industries</h5>
               <ul class="quicklinks">
                  <li><a href="industry/accounts.html" title="Accounts Jobs">Accounts Jobs</a></li>
                  <li><a href="industry/advertising.html" title="Advertising Jobs">Advertising Jobs</a></li>
                  <li><a href="industry/banking.html" title="Banking Jobs">Banking Jobs</a></li>
                  <li><a href="industry/customer-service.html" title="Customer Service Jobs">Customer Service Jobs</a></li>
                  <li><a href="industry/graphic-web-design.html" title="Graphic / Web Design Jobs">Graphic / Web Design Jobs</a></li>
                  <li><a href="industry/hr-industrial-relations.html" title="HR / Industrial Relations Jobs">HR / Industrial Relations Jobs</a></li>
                  <li><a href="industry/it-software.html" title="IT - Software Jobs">IT - Software Jobs</a></li>
                  <li><a href="industry/teaching-education.html" title="Teaching / Education Jobs">Teaching / Education Jobs</a></li>
                  <li><a href="industry/it-hardware.html" title="IT - Hardware Jobs">IT - Hardware Jobs</a></li>
               </ul>
            </div>
            <div class="col-md-4">
               <h5>Popular Cities</h5>
               <ul class="citiesList">
                  <li><a href="search/dubai.html" title="Jobs in Dubai">Dubai</a></li>
                  <li><a href="search/hong-kong.html" title="Jobs in Hong Kong">Hong Kong</a></li>
                  <li><a href="search/islamabad.html" title="Jobs in Islamabad">Islamabad</a></li>
                  <li><a href="search/las-vegas.html" title="Jobs in Las Vegas">Las Vegas</a></li>
                  <li><a href="search/new-york.html" title="Jobs in New York">New York</a></li>
               </ul>
               <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="copyright">
               <a href="interview.html" title="Preparing for Interview">Preparing for Interview</a> |
               <a href="cv-writing.html" title="CV Writing">CV Writing</a> |
               <a href="how-to-get-job.html" title="How to get Job">How to get Job</a> |
               <a href="privacy-policy.html" title="Policy">Policy</a>
               <div class="bttxt">Copyright 2019. Job Portal. Design & Develop by: <a href="http://codeareena.com/" target="_blank">Codeareena</a></div>
            </div>
         </div>
      </div>
      </div>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="public/js/jquery-1.11.0.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="public/js/bootstrap.min.js"></script>
      <script src="public/js/bootbox.min.js"></script>
      <script src="public/js/functions.js" type="text/javascript"></script>
      <script src="public/js/validation.js" type="text/javascript"></script>
      <script src="public/js/validate_jobseeker.js" type="text/javascript"></script>
      <script src="public/autocomplete/jquery-1.4.4.js"></script>
      <script src="public/autocomplete/jquery.ui.core.js"></script>
      <script src="public/autocomplete/jquery.ui.widget.js"></script>
      <script src="public/autocomplete/jquery.ui.button.js"></script>
      <script src="public/autocomplete/jquery.ui.position.js"></script>
      <script src="public/autocomplete/jquery.ui.autocomplete.js"></script>
      <script type="text/javascript"> var cy = ''; </script>
      <script src="public/autocomplete/action-js.js"></script>
      <script type="text/javascript">
         $(document).ready(function () {
           var navListItems = $('div.setup-panel div a'),
                   allWells = $('.setup-content'),
                   allNextBtn = $('.nextBtn');

           allWells.hide();

           navListItems.click(function (e) {
               e.preventDefault();
               var $target = $($(this).attr('href')),
                       $item = $(this);

               if (!$item.hasClass('disabled')) {
                   navListItems.removeClass('btn-primary').addClass('btn-default');
                   $item.addClass('btn-primary');
                   allWells.hide();
                   $target.show();
                   $target.find('input:eq(0)').focus();
               }
           });

           allNextBtn.click(function(){
               var curStep = $(this).closest(".setup-content"),
                   curStepBtn = curStep.attr("id"),
                   nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                   curInputs = curStep.find("input[type='text'],input[type='url']"),
                   isValid = true;

               $(".form-group").removeClass("has-error");
               for(var i=0; i<curInputs.length; i++){
                   if (!curInputs[i].validity.valid){
                       isValid = false;
                       $(curInputs[i]).closest(".form-group").addClass("has-error");
                   }
               }

               if (isValid)
                   nextStepWizard.removeAttr('disabled').trigger('click');
           });

           $('div.setup-panel div a.btn-primary').trigger('click');
         });
      </script>
   </body>
</html>

