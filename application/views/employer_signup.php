
<div class="topheader">
  <div class="navbar navbar-default" role="navigation">
        <div class="col-md-2">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.html"><img src="public/images/logo.png" /></a> </div>
        </div>
        <div class="col-md-6">
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                            <li class="inactive"><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i></a></li>
              <li class="inactive"><a href="search-jobs.html" title="Search Government jobs in USA">Search a Job</a> </li>
              <li class="inactive"><a href="search-resume.html" title="Search Resume">Search Resume</a></li>
              <li class="inactive"><a href="about-us.html" title="USA jobs free website">About Us</a></li>
              <li class="inactive"><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
                          </ul>
          </div>
        </div>
        <!--/.nav-collapse -->
        
        <div class="col-md-4">
          <div class="usertopbtn">
		            
          <a href="employer-signup.html" class="lookingbtn">Post a Job</a>
          <a href="jobseeker-signup.html" class="hiringbtn">Job Seeker</a>
          <a href="login.html" class="loginBtn" title="Jobs openings">Login</a>
                    <div class="clear"></div>
          </div>
        </div>
		
        <div class="clearfix"></div>
  </div>
</div>
<!--/Header-->
<div class="container detailinfo">
  <div class="row"> <form action="http://codeareena.com/demo/jobportal/employer_signup" name="emp_form" id="emp_form" onSubmit="return validate_employer_form(this);" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    <div class="col-md-10">
      <p>With Job Portal, the employer signup process only takes a couple of minutes and once your registration is complete, you have access to search the Job Seekers and post the Job openings in your company. The candidates who have opted to receive Job Alerts will receive your Job Opening in their Email. If you have an opening in your company, don't spend thousands of rupees advertising in newspaper or websites that charge you. Give us a chance to provide you top quality service for free!</p>
      <h2> Create New Account</h2>
      
      <!--Account info-->
      <div class="formwraper">
        <div class="titlehead">Account Information</div>
        <div class="formint">
          <div class="input-group ">
            <label class="input-group-addon">Email <span>*</span></label>
            <input name="email" type="text" class="form-control" id="email" placeholder="Email" value="" maxlength="150">
             </div>
          <div class="input-group ">
            <label class="input-group-addon">Password <span>*</span></label>
            <input name="pass" type="password" class="form-control" id="pass" placeholder="Password" value="" maxlength="100">
             </div>
          <div class="input-group ">
            <label class="input-group-addon">Confirm Password <span>*</span></label>
            <input name="confirm_pass" type="password" class="form-control" id="confirm_pass" placeholder="Confirm Password" value="" maxlength="100">
             </div>
        </div>
      </div>
      
      <!--Personal info-->
      <div class="formwraper">
        <div class="titlehead">Company Information</div>
        <div class="formint">
          <div class="input-group ">
            <label class="input-group-addon">Your Name <span>*</span></label>
            <input name="full_name" type="text" class="form-control" id="full_name" placeholder="Full Name" value="" maxlength="40">
             </div>
          <div class="input-group ">
            <label class="input-group-addon">Company Name <span>*</span></label>
            <input name="company_name" type="text" class="form-control" id="company_name" value="" maxlength="50" />
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Industry <span>*</span></label>
            <select name="industry_id" id="industry_id" class="form-control" style="max-width:350px;">
              <option value="" selected>Select Industry</option>
                            <option value="3" >Accounts</option>
                            <option value="5" >Advertising</option>
                            <option value="7" >Banking</option>
                            <option value="10" >Customer Service</option>
                            <option value="16" >Graphic / Web Design</option>
                            <option value="18" >HR / Industrial Relations</option>
                            <option value="40" >IT - Hardware</option>
                            <option value="22" >IT - Software</option>
                            <option value="35" >Teaching / Education</option>
                          </select>
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Org. Type </label>
            <select class="form-control" name="ownership_type" id="ownership_type">
              <option value="Private">Private</option>
              <option value="Public">Public</option>
              <option value="Government">Government</option>
              <option value="Semi-Government">Semi-Government</option>
              <option value="NGO">NGO</option>
            </select>
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Address <span>*</span></label>
            <textarea class="form-control" name="company_location" id="company_location" ></textarea>
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Location <span>*</span></label>
            <select name="country" id="country" class="form-control" onChange="grab_cities_by_country(this.value);" style="width:50%">
                            <option value="Australia" >Australia</option>
                            <option value="Canada" >Canada</option>
                            <option value="China" >China</option>
                            <option value="Egypt" >Egypt</option>
                            <option value="Pakistan" >Pakistan</option>
                            <option value="Sweden" >Sweden</option>
                            <option value="United Arab Emirates" >United Arab Emirates</option>
                            <option value="United Kingdom" >United Kingdom</option>
                            <option value="United States" >United States</option>
                          </select>
                       
            <div class="demo">
              <input name="city" type="text" class="form-control" id="city_text" style="max-width:165px; " value="" maxlength="50">
            </div>
            
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Landline Phone <span>*</span></label>
            <input type="phone" class="form-control" name="company_phone" id="company_phone" value="" maxlength="20" />
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Cell Phone <span>*</span></label>
            <input name="mobile_phone" type="text" class="custom-control" id="mobile_phone" value="" maxlength="15" />
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Company Website <span>*</span></label>
            <input name="company_website" type="text" class="form-control" id="company_website" value="" maxlength="155">
             </div>
          
          
          <div class="input-group ">
            <label class="input-group-addon">No. of Employees <span>*</span></label>
            <select name="no_of_employees" id="no_of_employees" class="form-control">
              <option value="1-10" >1-10</option>
              <option value="11-50" >11-50</option>
              <option value="51-100" >51-100</option>
              <option value="101-300" >101-300</option>
              <option value="301-600" >301-600</option>
              <option value="601-1000" >601-1000</option>
              <option value="1001-1500" >1001-1500</option>
              <option value="1501-2000" >1501-2000</option>
              <option value="More than 2000" >More than 2000</option>
            </select>
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Company Description <span>*</span></label>
            <textarea class="form-control" name="company_description" id="company_description" ></textarea>
             </div>
          
          <div class="input-group ">
            <label class="input-group-addon">Company Logo <span>*</span></label>
            <input type="file" class="form-control" name="company_logo" id="company_logo" accept="image/*" />
            <p>Upload files only in .jpg, .jpeg, .gif or .png format with max size of 6 MB.</p>
             </div>
          
			 <div class="formsparator">
            <div class="input-group">
              <label class="input-group-addon">Please enter: <img id="909598503" src="public/images/captcha/1553409125.8836.jpg" style="width: 120; height: 40; border: 0;" alt=" " />  in the text box below:</label></div>
            <div class="input-group ">
              <label class="input-group-addon"></label>
              <input type="text" class="form-control" name="captcha" id="captcha" value="" maxlength="10" />
               </div>
              </div>
          <div align="center">
            <input type="submit" name="submit_button" id="submit_button" value="Sign Up" class="btn btn-success" />
          </div>
          
          
        </div>
      </div>
      
      <!--Professional info-->
      
    </div>
    <!--/Job Detail--> 
    </form>    <div class="col-md-2"><div class="clear">&nbsp;</div></div>  </div>
</div>
<div class="clear">&nbsp;</div>
<div class="text-center">
</div><!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="col-md-3">
      <img src="public/images/logo.png" alt="Job Portal" />
      <br><br>
      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
      
      <!--Social-->
        
        <div class="social">
        <a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a href="http://www.plus.google.com/" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
        <a href="http://www.facebook.com/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
        
      
    </div>
    <div class="col-md-2">
      <h5>Quick Links</h5>
      <ul class="quicklinks">
        <li><a href="about-us.html" title="About Us">About Us</a></li>
        <li><a href="how-to-get-job.html" title="How to get Job">How to get Job</a></li>
        <li><a href="search-jobs.html" title="New Job Openings">New Job Openings</a></li>
        <li><a href="search-resume.html" title="New Job Openings">Resume Search</a></li>
        <li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
      </ul>
    </div>
    
    <div class="col-md-3">
      <h5>Popular Industries</h5>
      <ul class="quicklinks">
                <li><a href="industry/accounts.html" title="Accounts Jobs">Accounts Jobs</a></li>
                <li><a href="industry/advertising.html" title="Advertising Jobs">Advertising Jobs</a></li>
                <li><a href="industry/banking.html" title="Banking Jobs">Banking Jobs</a></li>
                <li><a href="industry/customer-service.html" title="Customer Service Jobs">Customer Service Jobs</a></li>
                <li><a href="industry/graphic-web-design.html" title="Graphic / Web Design Jobs">Graphic / Web Design Jobs</a></li>
                <li><a href="industry/hr-industrial-relations.html" title="HR / Industrial Relations Jobs">HR / Industrial Relations Jobs</a></li>
                <li><a href="industry/it-software.html" title="IT - Software Jobs">IT - Software Jobs</a></li>
                <li><a href="industry/teaching-education.html" title="Teaching / Education Jobs">Teaching / Education Jobs</a></li>
                <li><a href="industry/it-hardware.html" title="IT - Hardware Jobs">IT - Hardware Jobs</a></li>
              </ul>
    </div>
    <div class="col-md-4">
      <h5>Popular Cities</h5>
      <ul class="citiesList">
              <li><a href="search/dubai.html" title="Jobs in Dubai">Dubai</a></li>
               <li><a href="search/hong-kong.html" title="Jobs in Hong Kong">Hong Kong</a></li>
               <li><a href="search/islamabad.html" title="Jobs in Islamabad">Islamabad</a></li>
               <li><a href="search/las-vegas.html" title="Jobs in Las Vegas">Las Vegas</a></li>
               <li><a href="search/new-york.html" title="Jobs in New York">New York</a></li>
         
      </ul>
      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
    <div class="copyright">
    <a href="interview.html" title="Preparing for Interview">Preparing for Interview</a> | 
    <a href="cv-writing.html" title="CV Writing">CV Writing</a> | 
    <a href="how-to-get-job.html" title="How to get Job">How to get Job</a> |
    <a href="privacy-policy.html" title="Policy">Policy</a>
    
    
    
      <div class="bttxt">Copyright 2019. Job Portal. Design & Develop by: <a href="http://codeareena.com/" target="_blank">Codeareena</a></div>
    </div>
  </div>
</div>
</div>
 <script src="public/js/bad_words.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="public/js/jquery-1.11.0.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/bootbox.min.js"></script>
<script src="public/js/functions.js" type="text/javascript"></script>
<script src="public/js/validation.js" type="text/javascript"></script>







<script src="public/js/validate_employer.js" type="text/javascript"></script> 
<script src="public/autocomplete/jquery-1.4.4.js"></script> 
<script src="public/autocomplete/jquery.ui.core.js"></script> 
<script src="public/autocomplete/jquery.ui.widget.js"></script> 
<script src="public/autocomplete/jquery.ui.button.js"></script> 
<script src="public/autocomplete/jquery.ui.position.js"></script> 
<script src="public/autocomplete/jquery.ui.autocomplete.js"></script> 
<script type="text/javascript"> var cy = ''; </script> 
<script src="public/autocomplete/action-js.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('button').css('display','none');
	if(cy!='USA' && cy!='')
		$(".ui-autocomplete-input.ui-widget.ui-widget-content.ui-corner-left").css('display','none');			
});
</script>
</body>

<!-- Mirrored from codeareena.com/demo/jobportal/employer-signup by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Mar 2019 06:35:43 GMT -->
</html>