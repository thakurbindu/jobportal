
<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="col-md-3">
      <img src="public/images/logo.png" alt="Job Portal" />
      <br><br>
      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
      
      <!--Social-->
        
        <div class="social">
        <a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a href="http://www.plus.google.com/" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
        <a href="http://www.facebook.com/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
        
      
    </div>
    <div class="col-md-2">
      <h5>Quick Links</h5>
      <ul class="quicklinks">
        <li><a href="about-us.html" title="About Us">About Us</a></li>
        <li><a href="how-to-get-job.html" title="How to get Job">How to get Job</a></li>
        <li><a href="search-jobs.html" title="New Job Openings">New Job Openings</a></li>
        <li><a href="search-resume.html" title="New Job Openings">Resume Search</a></li>
        <li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
      </ul>
    </div>
    
    <div class="col-md-3">
      <h5>Popular Industries</h5>
      <ul class="quicklinks">
                <li><a href="industry/accounts.html" title="Accounts Jobs">Accounts Jobs</a></li>
                <li><a href="industry/advertising.html" title="Advertising Jobs">Advertising Jobs</a></li>
                <li><a href="industry/banking.html" title="Banking Jobs">Banking Jobs</a></li>
                <li><a href="industry/customer-service.html" title="Customer Service Jobs">Customer Service Jobs</a></li>
                <li><a href="industry/graphic-web-design.html" title="Graphic / Web Design Jobs">Graphic / Web Design Jobs</a></li>
                <li><a href="industry/hr-industrial-relations.html" title="HR / Industrial Relations Jobs">HR / Industrial Relations Jobs</a></li>
                <li><a href="industry/it-software.html" title="IT - Software Jobs">IT - Software Jobs</a></li>
                <li><a href="industry/teaching-education.html" title="Teaching / Education Jobs">Teaching / Education Jobs</a></li>
                <li><a href="industry/it-hardware.html" title="IT - Hardware Jobs">IT - Hardware Jobs</a></li>
              </ul>
    </div>
    <div class="col-md-4">
      <h5>Popular Cities</h5>
      <ul class="citiesList">
              <li><a href="search/dubai.html" title="Jobs in Dubai">Dubai</a></li>
               <li><a href="search/hong-kong.html" title="Jobs in Hong Kong">Hong Kong</a></li>
               <li><a href="search/islamabad.html" title="Jobs in Islamabad">Islamabad</a></li>
               <li><a href="search/las-vegas.html" title="Jobs in Las Vegas">Las Vegas</a></li>
               <li><a href="search/new-york.html" title="Jobs in New York">New York</a></li>
         
      </ul>
      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
    <div class="copyright">
    <a href="interview.html" title="Preparing for Interview">Preparing for Interview</a> | 
    <a href="cv-writing.html" title="CV Writing">CV Writing</a> | 
    <a href="how-to-get-job.html" title="How to get Job">How to get Job</a> |
    <a href="privacy-policy.html" title="Policy">Policy</a>
    
    
    
      <div class="bttxt">Copyright 2019. Job Portal. Design & Develop by: <a href="#" target="_blank">ostazone</a></div>
    </div>
  </div>
</div>
</div>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo base_url();?>assets/js/jquery-1.11.0.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
<script src="<?php echo base_url();?>assets/js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/validation.js" type="text/javascript"></script>







<!-- FlexSlider --> 
<script src="<?php echo base_url();?>assets/js/jquery.flexslider.js" type="text/javascript"></script> 
<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 250,
    minItems: 1,
    maxItems: 1
  });
});
</script>
</body>

<!-- Mirrored from codeareena.com/demo/jobportal/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Mar 2019 06:33:17 GMT -->
</html>