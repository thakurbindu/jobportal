<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codeareena.com/demo/jobportal/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Mar 2019 06:31:50 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Jobs around the world</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
 <meta name="keywords" content="jobs in USA, government jobs, online jobs in USA, latest jobs in USA,  job in USA, latest jobs">
<title>Online Jobs around the world</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">

<link href='<?php echo base_url();?>assets/css/google-fonts.css' rel='stylesheet' type='text/css'>
<link href='<?php echo base_url();?>assets/css/flexslider.css' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src=""></script>
      <script src=""></script>
    <![endif]-->
<link rel="shortcut icon" href="public/images/favicon.ico">

</head>
<body>
 <div class="siteWraper">
<!--Header-->
<div class="topheader">
  <div class="navbar navbar-default" role="navigation">
        <div class="col-md-2">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="<?php base_url();?>"><img src="<?php echo base_url();?>assets/images/logo.png" /></a> </div>
        </div>
        <div class="col-md-6">
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                            <li class="active"><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i></a></li>
              <li class="inactive"><a href="search-jobs.html" title="Search Government jobs in USA">Search a Job</a> </li>
              <li class="inactive"><a href="search-resume.html" title="Search Resume">Search Resume</a></li>
              <li class="inactive"><a href="about-us.html" title="USA jobs free website">About Us</a></li>
              <li class="inactive"><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
                          </ul>
          </div>
        </div>
        <!--/.nav-collapse -->
        
        <div class="col-md-4">
          <div class="usertopbtn">
		            
          <a href="<?php echo base_url();?>employersignup" class="lookingbtn">Post a Job</a>
          <a href="<?php echo base_url();?>signup" class="hiringbtn">Job Seeker</a>
          <a href="<?php echo base_url();?>login" class="loginBtn" title="Jobs openings">Login</a>
                    <div class="clear"></div>
          </div>
        </div>
		
        <div class="clearfix"></div>
  </div>
</div>
<!--/Header--> 