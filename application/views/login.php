<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codeareena.com/demo/jobportal/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Mar 2019 06:35:44 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Apply for jobs</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
 <title>Apply for jobs</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">

<link href='public/css/google-fonts.css' rel='stylesheet' type='text/css'>
<link href='public/css/flexslider.css' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link href="public/css/bootstrap.min.css" rel="stylesheet">
<link href="public/css/font-awesome.css" rel="stylesheet" type="text/css" />

<link href="public/css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src=""></script>
      <script src=""></script>
    <![endif]-->
<link rel="shortcut icon" href="public/images/favicon.ico">
<script>
var baseUrl = 'index.html';
</script>
</head>
<body>
 <div class="siteWraper">
<!--Header-->
<div class="topheader">
  <div class="navbar navbar-default" role="navigation">
        <div class="col-md-2">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.html"><img src="public/images/logo.png" /></a> </div>
        </div>
        <div class="col-md-6">
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                            <li class="inactive"><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i></a></li>
              <li class="inactive"><a href="search-jobs.html" title="Search Government jobs in USA">Search a Job</a> </li>
              <li class="inactive"><a href="search-resume.html" title="Search Resume">Search Resume</a></li>
              <li class="inactive"><a href="about-us.html" title="USA jobs free website">About Us</a></li>
              <li class="inactive"><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
                          </ul>
          </div>
        </div>
        <!--/.nav-collapse -->
        
        <div class="col-md-4">
          <div class="usertopbtn">
		            
          <a href="employer-signup.html" class="lookingbtn">Post a Job</a>
          <a href="jobseeker-signup.html" class="hiringbtn">Job Seeker</a>
          <a href="login.html" class="loginBtn" title="Jobs openings">Login</a>
                    <div class="clear"></div>
          </div>
        </div>
		
        <div class="clearfix"></div>
  </div>
</div>
<!--/Header--> 
<!--Detail Info-->
<div class="container innerpages">
 <div class="clear">&nbsp;</div>
<div class="text-center">
</div> 
  <div class="row"> 
    
    <!--Signup-->
    <div class="col-md-6 col-md-offset-3">
     <!--Login-->
    <form name="login_form" id="login_form" action="#" method="post">
      <div class="loginbox">
        <h3>Sign Up With</h3>
                                <div class="row">
          <div class="col-md-12">
            <label class="input-group-addon">Email <span>*</span></label>
          </div>
          <div class="col-md-12">
            <input type="text" name="email" id="email" class="form-control" value="" placeholder="Email" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <label class="input-group-addon">Password <span>*</span></label>
          </div>
          <div class="col-md-12">
            <input type="password" name="pass" id="pass" autocomplete="off" value="" class="form-control" placeholder="Password" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <input type="checkbox">
            Remember My Password</div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <input type="submit" value="Sign In" class="btn btn-success" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">Forgot Your Password? <a href="forgot.html">Click Here</a></div>
        </div>
      </div>
    </form>
    
      <div class="signupbox">
        <h4>Not a member yet? Click Below to <a href="jobseeker-signup.html" > Sign Up </a></h4>
        <div class="clear"></div>
      </div>
    </div>
    <!--/Login--> 

  </div>
</div>
<div class="clear">&nbsp;</div>
<div class="text-center">
</div><!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="col-md-3">
      <img src="public/images/logo.png" alt="Job Portal" />
      <br><br>
      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
      
      <!--Social-->
        
        <div class="social">
        <a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a href="http://www.plus.google.com/" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
        <a href="http://www.facebook.com/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
        
      
    </div>
    <div class="col-md-2">
      <h5>Quick Links</h5>
      <ul class="quicklinks">
        <li><a href="about-us.html" title="About Us">About Us</a></li>
        <li><a href="how-to-get-job.html" title="How to get Job">How to get Job</a></li>
        <li><a href="search-jobs.html" title="New Job Openings">New Job Openings</a></li>
        <li><a href="search-resume.html" title="New Job Openings">Resume Search</a></li>
        <li><a href="contact-us.html" title="Contact Us">Contact Us</a></li>
      </ul>
    </div>
    
    <div class="col-md-3">
      <h5>Popular Industries</h5>
      <ul class="quicklinks">
                <li><a href="industry/accounts.html" title="Accounts Jobs">Accounts Jobs</a></li>
                <li><a href="industry/advertising.html" title="Advertising Jobs">Advertising Jobs</a></li>
                <li><a href="industry/banking.html" title="Banking Jobs">Banking Jobs</a></li>
                <li><a href="industry/customer-service.html" title="Customer Service Jobs">Customer Service Jobs</a></li>
                <li><a href="industry/graphic-web-design.html" title="Graphic / Web Design Jobs">Graphic / Web Design Jobs</a></li>
                <li><a href="industry/hr-industrial-relations.html" title="HR / Industrial Relations Jobs">HR / Industrial Relations Jobs</a></li>
                <li><a href="industry/it-software.html" title="IT - Software Jobs">IT - Software Jobs</a></li>
                <li><a href="industry/teaching-education.html" title="Teaching / Education Jobs">Teaching / Education Jobs</a></li>
                <li><a href="industry/it-hardware.html" title="IT - Hardware Jobs">IT - Hardware Jobs</a></li>
              </ul>
    </div>
    <div class="col-md-4">
      <h5>Popular Cities</h5>
      <ul class="citiesList">
              <li><a href="search/dubai.html" title="Jobs in Dubai">Dubai</a></li>
               <li><a href="search/hong-kong.html" title="Jobs in Hong Kong">Hong Kong</a></li>
               <li><a href="search/islamabad.html" title="Jobs in Islamabad">Islamabad</a></li>
               <li><a href="search/las-vegas.html" title="Jobs in Las Vegas">Las Vegas</a></li>
               <li><a href="search/new-york.html" title="Jobs in New York">New York</a></li>
         
      </ul>
      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
    <div class="copyright">
    <a href="interview.html" title="Preparing for Interview">Preparing for Interview</a> | 
    <a href="cv-writing.html" title="CV Writing">CV Writing</a> | 
    <a href="how-to-get-job.html" title="How to get Job">How to get Job</a> |
    <a href="privacy-policy.html" title="Policy">Policy</a>
    
    
    
      <div class="bttxt">Copyright 2019. Job Portal. Design & Develop by: <a href="http://codeareena.com/" target="_blank">Codeareena</a></div>
    </div>
  </div>
</div>
</div>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="public/js/jquery-1.11.0.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/bootbox.min.js"></script>
<script src="public/js/functions.js" type="text/javascript"></script>
<script src="public/js/validation.js" type="text/javascript"></script>







</body>

<!-- Mirrored from codeareena.com/demo/jobportal/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Mar 2019 06:35:45 GMT -->
</html>